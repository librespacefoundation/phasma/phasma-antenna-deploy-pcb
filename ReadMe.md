# Antenna HDRM electronics

### Interface Documentation

This is a brief explanation of the interface of the HDRM. 

The antenna HDRM electronics system contains the main electronics mechanism for the deployment of the uhf & GNSS antenna, as well as the fine sun sensor. Two thermal knifes (low ohmage through-hole resistors) are used to burn a thread that keeps the cap closed. One thermal knife is activated directly from a commanded 5V power supply. The other thermal knife uses a MOSFET to burn the resistor. It requires a constant 5V power supply and a 3v3 digital command. The activation of only one of the above is sufficient to burn the thread. Finally, the constant 5V power supply is also used in the feedback mechanism. The feedback mechanism uses a voltage divider which converts the 5V into 1V when the antenna is stowed, 2V when it is partially deployed and 3.3V when is fully deployed. The output of the voltage divider is used as an analog output pin.

The connector used between the HDRM and the satellite is a 6 pin angled picoblade 1.25mm  (53261-0671). It can withstand up to 1A per pin. 

A pinout table is shown below.

| Pinout                     |       Overview                                             |
| --------------             |  -------------------------------------                     |
| Digital Input 3v3          |   A 3.3V command to turn the MOSFET on                     |
| Analog Output 3v3          |   An output with three distinct levels (1.0V, 2.0V, 3.3V)  |
| GND                        |   Ground                                                   |
| Power Supply 5V            |   A constant power supply of 5 Volts                       |
| Commanded Power Supply 5V  |   A power supply of 5 Volts that can be turned on and off  |
| GND                        |   Ground                                                   |

A state table of the feedback mechanism is shown below.

| Feedback Voltage           |       Status                                               |
| --------------             |  -------------------------------------                     |
| 1.0V                       |   Stowed                                                   |
| 2.0V                       |   Partially deployed                                       |
| 3.3V                       |   Deployed                                                 |


### Current and power ratings

- I ~= 420mA per thermal knife (per line) -> ~ 840mA total
- P ~= 2W per thermal knife (per line) -> ~ 4W total
- PTFE Cable AWG-26 for power lines
- PTFE Cable AWG-28 for low power lines

### Revision Changes

#### v0.2

Bottom PCB:

- TBD

Top PCB:

- Added ground plane on bottom side
- Added glue spot area

Top PCB FSS:

- Identical to TOP PCB without the ground planes

Antenna PCB:

- No changes


#### v0.1

Bottom PCB:

- Changed 15 Ohm to 12 Ohm
- Changed position of the SW2 switch
- Removed silkscreen from the copper zones

Top PCB:

- Added 6 mounting holes in the edges of the PCB

Antenna PCB:

- Moved copper zones to exposed layer

